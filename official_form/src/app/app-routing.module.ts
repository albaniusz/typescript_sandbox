import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HeroFormComponent} from './hero-form/hero-form.component';


const routes: Routes = [
  {path: 'form', component: HeroFormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
